//
//  HomeViewController.swift
//  prado_roberto_ios_2018_b
//
//  Created by PRADOPC on 11/27/18.
//  Copyright © 2018 PRADOPC. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {
    @IBOutlet weak var EnteroTextField: UITextField!
    @IBOutlet weak var resultadoTextView: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func OpenLoginButtonPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func ParesButtonPressed(_ sender: Any) {
        let a:Int = Int(EnteroTextField.text ?? "0") ?? 0
        print(a)
        getPares(number: a)
    }
    func getPares(number: Int) {
        var limit = (number * 2) - 1
        if limit < 0 {
            limit = 0
        }
        let pares = (0...limit).filter { $0 % 2 == 0 }
        var res = ""
        for n in pares{
            res += String(n) + ","
        }
        resultadoTextView.text = res
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
