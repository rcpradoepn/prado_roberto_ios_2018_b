//
//  ViewController.swift
//  prado_roberto_ios_2018_b
//
//  Created by PRADOPC on 11/27/18.
//  Copyright © 2018 PRADOPC. All rights reserved.
//

import UIKit
import FirebaseAuth
class ViewController: UIViewController {
    @IBOutlet weak var emailTextField: UITextField!
    
    @IBOutlet weak var passwordTextField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    @IBAction func loginButtonPressed(_ sender: Any) {
        let username = emailTextField.text!
        let password = passwordTextField.text!
        Auth.auth().signIn(withEmail: username, password: password) { (data, error) in
            if let error = error {
                print(error)
                return
            }
            // print("Welcome!")
            self.performSegue(withIdentifier: "go_home", sender: self)
        }
    }
    
}

